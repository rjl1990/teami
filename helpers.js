var fs = require('fs');

function readFile (filePath) {
    return new Promise ((resolve, reject) => {
        fs.readFile(filePath, 'utf-8', (err, fd) => {
            if (err) {
                var error = 'readFile: '+filePath+' Does not exist'; 
                return reject(error);
            }
            return resolve(fd);
        });
    })
}

function writeStream (toStream, toWrite) {
    var toStream = fs.createWriteStream(toStream);
    toStream.write(toWrite);
}

function parseToJson (el) {
    return JSON.parse(el);
}

function returnDates (date) {
    if (Object.prototype.toString.call(date) == '[object Date]') {
        return date
    }
    var parts = date.split('-');
    var mydate = new Date(parts[2]+"-"+parts[1]+"-"+parts[0]); 
    return mydate
}

function compareDates (date1, date2) { // if date1 is > date2
    if (Object.prototype.toString.call(date1) == '[object Date]' && Object.prototype.toString.call(date2) == '[object Date]') {
        if (date1 > date2) {
            return true
        } else {
            return false
        }
    }
}


module.exports = {
    readFile,
    writeStream,
    parseToJson,
    returnDates,
    compareDates
}