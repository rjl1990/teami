var helpers = require('./helpers.js');

function getPeopleandProjects () { // returns a promise with an object for people and projects from a file
   return new Promise ((resolve,reject) => {
    Promise.all([
            helpers.readFile('./people.json'), 
            helpers.readFile('./projects.json')
        ])
        .then((fileData)=>{
            var people = helpers.parseToJson(fileData[0]),
                projects = helpers.parseToJson(fileData[1]);
            resolve({"people":people, "projects":projects});
        })
        .catch((error) => {
            reject(error);
        })
    });
}

function returnDayType (date) {
    date = date.getDay();
    switch (date) {
        case 0:
            return "weekend";
        case 1:
            return "work";
        case 2:
            return "work";
        case 3:
            return "work";
        case 4:
            return "work";
        case 5:
            return "work";
        case 6: 
            return "weekend";
        default: 
            return 'error in ReturnDayType';
    }
}

function workDayAllowedCheck (date) {
    
    if (returnDayType(date) != 'work') {
        return false;
    }
    return true;
}

function countWorkingDays (start, end) {
    var start = helpers.returnDates(start),
        end = helpers.returnDates(end),
        dayCount= 0;
    for (var date = start; date <= end; date.setDate(date.getDate() + 1)) {
        
        if (workDayAllowedCheck(date) === true) {
            dayCount++;
        }
    }
    return dayCount
}

function createSchedule (schedulestart, scheduleend) { // Returns schedule dates with default settings
    var scheduleDates = {};
    for (var date = new Date (schedulestart); date <= scheduleend; date.setDate(date.getDate() + 1)) {
        var type = returnDayType(date),
            allowed = workDayAllowedCheck(date),
            storedate = date.toUTCString();

        scheduleDates[storedate] = {
            allowedHours: allowed,
            projectRequiredHours: 0,
            teamAvailableHours: 0,
            dayType: type
        }
    }
    return scheduleDates;
}

function createScheduleData(projects) { // Returns the schedule including, start, end and scheduleDates objects
    return new Promise ((resolve, reject) => {
        if (typeof projects == "object") {
            var schedule = {};  
            for (var x = 0; x < projects.length; x++) {
                var newStartDate = helpers.returnDates(projects[x].start),
                    newEndDate = helpers.returnDates(projects[x].end);
                if (!schedule.start) {
                    schedule.start = newStartDate;
                }
                if (!schedule.end) {
                    schedule.end = newEndDate;
                }
                
                var currentStartDate = helpers.returnDates(schedule.start),
                    currentEndDate = helpers.returnDates(schedule.end);
                
                if (newStartDate < currentStartDate) {
                    schedule.start = newStartDate;
                }
                if (newEndDate > currentEndDate) {
                    schedule.end = newEndDate;
                }
            }
            schedule.scheduleDates = createSchedule(schedule.start, schedule.end);
            resolve(schedule);
        } else {
            reject("Projects is not an object");
        }
    });
}

function createPeopleandProjectsObject () {
    return new Promise ((resolve,reject) => {
        getPeopleandProjects()
        .then(data => {
            return new Promise ((resolve,reject) => {
                createScheduleData(data.projects)
                .then((schedule) => {
                    data.schedule = schedule;
                    resolve(data);
                })
            })
        })
        .then(data => {
            resolve(data);
        })
        .catch(error => {
            reject(error)
        })
    })
}

function attributeProjects (project, mainObject) {
    var startDate = helpers.returnDates(project.start),
        endDate = helpers.returnDates(project.end),
        dailyhours = project.projecthours / countWorkingDays(project.start, project.end);

    for (var date = startDate; date <= endDate; date.setDate(date.getDate() + 1)) {
        if (mainObject.schedule.scheduleDates[date.toUTCString()].allowedHours === true){
            mainObject.schedule.scheduleDates[date.toUTCString()].projectRequiredHours = parseFloat(mainObject.schedule.scheduleDates[date.toUTCString()].projectRequiredHours + dailyhours).toFixed(2);
        }
    }
    return mainObject;
}

function checkPersonDisAllowedDay(person, date) {
    return true;
}

function attributePeople (person, mainObject) {
    var endDate = helpers.returnDates(mainObject.schedule.end),
        dailyhours = person.dailyHours;
    for (var date = new Date (mainObject.schedule.start); date <= endDate; date.setDate(date.getDate() + 1)) {
        if (mainObject.schedule.scheduleDates[date.toUTCString()].allowedHours === true){
            if (checkPersonDisAllowedDay(person, date)) {
                mainObject.schedule.scheduleDates[date.toUTCString()].teamAvailableHours = (parseFloat(mainObject.schedule.scheduleDates[date.toUTCString()].teamAvailableHours) + dailyhours).toFixed(2); 
            }
        }
    }
    return mainObject;
}

function forGoogleGraph (data) {
    var schedule = data.schedule.scheduleDates,
        array = []

    for (x in schedule) {
        var required = parseInt(schedule[x].projectRequiredHours);
        var team = parseInt(schedule[x].teamAvailableHours);
        array.push([x.replace(" 00:00:00 GMT",""), required, team])
    }
    return array
}

createPeopleandProjectsObject().then((data) => {
    return new Promise ((resolve, reject) => {
        for (var x = 0; x < data.projects.length; x++) {
           var newdata = attributeProjects(data.projects[x], data);
        }
        resolve(newdata);
    })
})
.then((data) => {

    return new Promise ((resolve, reject) => {
        for (var x = 0; x < data.people.length; x++) {
            var newdata = attributePeople(data.people[x], data);    
        }
        resolve(newdata)
    })
}).then((data) => {
    var information = forGoogleGraph(data)
    helpers.writeStream('./output.json', JSON.stringify(data));
    helpers.writeStream('./outputforGoogle.json', JSON.stringify(information));
})